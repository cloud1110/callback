# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.http import JsonResponse
from models import CallBackData
from sign import verify_sign

class APIView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(APIView, self).dispatch(request, *args, **kwargs)


class ReceiveList(View):
    def get(self, request):
        data_list = CallBackData.objects.all()
        content = {
            "data_list": data_list
        }
        return render(request, "api/receive.html", content)


class EasyTestAPI(APIView):
    def post(self, request):
        try:
            data = json.loads(request.body)
            sign = data.get("sign", "")
            if sign:
                data.pop("sign")
                if verify_sign(data, sign):
                    CallBackData.objects.create(data=request.body, sign=sign, verified=True)
                else:
                    CallBackData.objects.create(data=request.body, sign=sign, verified=False)
            else:
                CallBackData.objects.create(data=request.body)
            return JsonResponse({"status": "success"})
        except Exception as e:
            print e
            return JsonResponse({"status": "fail"}, status=401)


