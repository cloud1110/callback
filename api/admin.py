# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import CallBackData


class CallBackDataAdmin(admin.ModelAdmin):
    list_display = ("data", "source", "sign", "verified")


admin.site.register(CallBackData, CallBackDataAdmin)
