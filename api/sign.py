# -*- coding: utf-8 -*-
import hashlib
import json
import time
from django.conf import settings


def md5_sign(data):
    return hashlib.md5(data).hexdigest()


def tenant_api_sign(dict_data, key):
    add_timestamp_data = dict_data.update({"timestamp": time.time()})
    signed_data = add_timestamp_data.update(md5_sign(json.dumps(add_timestamp_data.update({"key": key}))))
    return signed_data


def verify_sign(dict_data, sign):
    key = settings.KAOLA_KEY
    dict_data.update({"key": key})
    if md5_sign(json.dumps(dict_data, sort_keys=True, separators=(',', ':'))) == sign:
        return True
    else:
        return False
