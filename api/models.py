# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class CallBackData(models.Model):
    data = models.CharField("receive data", max_length=200)
    source = models.CharField("source url", max_length=100)
    sign = models.CharField("sign data", max_length=100)
    verified = models.BooleanField("verify status", default=True)

