# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-04 07:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='callbackdata',
            name='sign',
            field=models.CharField(default='', max_length=100, verbose_name='sign data'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='callbackdata',
            name='verified',
            field=models.BooleanField(default=True, verbose_name='verify status'),
        ),
    ]
