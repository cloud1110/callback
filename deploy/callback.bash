#!/bin/bash

NAME="callback"
DJANGODIR=/home/ata/www/callback
DJANGO_SETTINGS_MODULE=callback.settings
DJANGO_WSGI_MODULE=callback.wsgi

echo "Starting $NAME as 'whoami'"

cd $DJANGODIR
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

exec ../env/callback/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
    -c deploy/gunicorn_settings.py
